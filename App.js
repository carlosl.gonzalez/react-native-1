import React, { Component } from 'react';
import { Alert, StyleSheet, Text, View, TextInput, Button, StatusBar} from 'react-native';
import Service from './Factory/Services'
import Dashboard from './Views/Dashboard'
import RegisterForm from './Views/Register'
import Login from './Views/Login'
import { COLOR } from 'react-native-material-ui';

export default class App extends React.Component {
  constructor (props) {
    super(props);
    this.customViews = {
      Register: 'RegisterForm',
      Login: 'LoginForm'
    };
    this.state = {
      userIsLoggedIn: false,
      token: '',
      user: {},
      isLoading: false,
      errorConnection: '',
      currentView: 'Dashboard'
    };
  }
  updateLoading (isLoading) {
    this.setState({isLoading: isLoading});
  }
  updateUserInfo (info) {
    this.setState({
      user: info.user,
      token: info.token,
      userIsLoggedIn: info.userIsLoggedIn
    });
  }
  updateView (view) { 
    this.setState({currentView: view})
  }
  updateError (err) {
    this.setState({errorConnection: err});
  }
  logout () {
    this.setState({
      user: {},
      token: '',
      currentView: 'Login'
    });
  }
  render() {
    console.log("APP/CurrentView: ", this.state.currentView);
    if (this.state.isLoading) {
      return (
        <View style={styles.mainStyle}>
          <StatusBar hidden />
          <Loading/>
        </View>
      );
    }
    if (this.state.errorConnection) {
      return (
        <View style={styles.mainStyle}>
          <StatusBar hidden />
          <Error error={this.state.errorConnection}/>
        </View>        
      );
    }
    if (this.state.currentView === 'Login') {
      return (
        <View style={styles.mainStyle}>
          <StatusBar hidden />
          <Login 
            isLoading={this.updateLoading.bind(this)}
            updateUserInfo={this.updateUserInfo.bind(this)}
            onError={this.updateError.bind(this)}
            setView={this.updateView.bind(this)}
          /> 
        </View> 
      );
    }
    if (this.state.currentView === 'Register') {
      return (
        <View style={styles.mainStyle}>
          <StatusBar hidden />
          <RegisterForm onReturn={this.updateView.bind(this)}/>
        </View>
      );
    }
    if (this.state.currentView === 'Dashboard') {
      return (
        <View style={styles.mainStyle}>
          <StatusBar hidden />
          <Dashboard userInformation={this.state.user} onReturn={this.updateView.bind(this)}
                     logout={this.logout.bind(this)}/>
        </View>      
      );
    }
  }
}

function Loading (props) {
  return (
    <View style={{flex:1}}>
      <View style={{flex:2}}/>
      <View style={{flex:1}}>
        <Text style={{textAlign: 'center'}}> Cargando... </Text>
      </View>
      <View style={{flex:2}}/>
    </View>
  );
}
function Error (props) {
  let err;
  if (!(typeof(props.err) === 'string')) {
    err = JSON.stringify(props.err);
  } else {
    err = props.err;
  }
  return (
    <View style={{padding: 60, backgroundColor: 'red'}}>
      <StatusBar hidden />
      <Text style={{color: 'black', fontSize: 20, fontWeight: 'bold'}}>{err}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  mainStyle: {
    flex: 1, 
    backgroundColor: COLOR.grey400,
  }
});