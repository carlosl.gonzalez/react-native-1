import React from 'react';
import { Alert, StyleSheet, Text, View, TextInput } from 'react-native';
import { Button, FormLabel, FormInput } from 'react-native-elements'
import DatePicker from 'react-native-datepicker'
import Login from './Login'

export default class RegisterForm  extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      user: {
        name: '',
        email: '',
        dateOfBorn: '',
      },
      FormValidationMessage: {
        name: '',
        email: '',
        date: ''
      }
    }
  }
  register () {
    let {eName, eEmail, eDate} = this.state.FormValidationMessage,
        {name, email, dateOfBorn} = this.state.user;
    if (eName || eEmail || eDate) {
      console.log("Formulario invalido");
      return;
    } else if (!name || !email || !dateOfBorn) {
      console.log("Todos los campos son necesarios");
      return;
    }
    console.log("user en state", this.state.user);
  }
  validInput (text, type) {
    let FormValidationMessage = {...this.state.FormValidationMessage},
        regex = {
          name: /^[a-z\ A-Z]+$/,
          email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        },
        messages = {
          name: "Nombre inválido",
          email: "Correo inválido"
        };
    if (!text) {
      FormValidationMessage[type] = "Debe llenar este campo"; 
      this.setState({ FormValidationMessage });
      return;
    }
    if (!text.match(regex[type])) {
      FormValidationMessage[type] = messages[type]; 
      this.setState({ FormValidationMessage });
    } else {
      FormValidationMessage[type] = ""; 
      this.setState({ FormValidationMessage });
      let user = {...this.state.user};
      user[type] = text;
      this.setState({ user })
    }
  }
  validDate (dateOfBorn) {
    let FormValidationMessage = { ...this.state.FormValidationMessage }; 
    if (Math.abs(Date.now() - new Date(dateOfBorn).getTime()) < 18 * 365 * 24 * 3600000) {
      FormValidationMessage.date = 'Debes tener más de 18 años para registrarte';
      this.setState({ FormValidationMessage });
    } else {
      FormValidationMessage.date = "";
      this.setState({ FormValidationMessage });
      let user = { ...this.state.user };
      user.dateOfBorn = new Date(dateOfBorn);
      this.setState({ user });
    }
  }
  goBack () {
    console.log ("Epa", Login)
    return <Login></Login>;
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.titleText}>Registro de usuario</Text>
        <View style={styles.form}>
          <FormLabel labelStyle={styles.labelStyle}>Nombre</FormLabel>
          <FormInput onChangeText={(text) => this.validInput(text, 'name')} inputStyle={styles.inputStyle}/>
          <FormValidationMessage errorMessage={this.state.FormValidationMessage.name}></FormValidationMessage>

          <FormLabel labelStyle={styles.labelStyle}>Correo</FormLabel>
          <FormInput onChangeText={(text) => this.validInput(text, 'email')} inputStyle={styles.inputStyle}/>
          <FormValidationMessage errorMessage={this.state.FormValidationMessage.email}></FormValidationMessage>

          <FormLabel labelStyle={styles.labelStyle}>Fecha de nacimiento</FormLabel>
          <DatePicker
            style={{width: 200}}
            date={this.state.dateOfBorn || new Date()}
            mode="date"
            placeholder="Seleccione una fecha"
            format="YYYY-MM-DD"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            maxDate={new Date()}
            minDate={new Date(new Date().getTime() - 99*365*24*3600000)}
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 20 + 20
              },
              dateInput: {
                marginLeft: 56 + 40,
                borderColor: 'steelblue'
              },
              dateText: {
                fontSize: 18,
                fontWeight: 'bold'
              }
            }}
            onDateChange={(date) => this.validDate(date)}
          />
          <FormValidationMessage errorMessage={this.state.FormValidationMessage.date}></FormValidationMessage>
        </View>
        <View style={{flexDirection: 'row', justifyContent:'center'}}>
        <Button 
            buttonStyle={styles.buttonDefault} title="Volver" 
            rounded={true} onPress={() => this.props.onReturn('Login')}
            textStyle={{fontWeight: 'bold', fontSize: 18, color: 'black'}}
          >
          </Button>
          <Button 
            buttonStyle={styles.buttonDefault} title="Enviar" 
            rounded={true} onPress={() => this.register()}
            textStyle={{fontWeight: 'bold', fontSize: 18, color: 'black'}}
          >
          </Button>
        </View>
      </View>
    );
  }
}

function FormValidationMessage (props) {
  return (
    <Text style={{color: 'tomato', fontSize: 12, fontWeight: 'bold', textAlign: 'right'}}>
      {props.errorMessage}
    </Text>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40
  },
  form: {
    padding: 20,    
  },
  buttonDefault: {
    marginTop: 20,
    backgroundColor: 'white',
    opacity: 0.5,
    elevation: 10,
  },
  alternativeLayoutButtonContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  appTitle: {
    color: 'white',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold'   
  },
  titleContainer: {
    height:100, 
    backgroundColor:'black'
  },
  titleText: {
    fontSize: 30, 
    textAlign: 'center', 
    color: 'white',
    fontWeight: 'bold'
  },
  labelStyle: {
    color: 'white'
  },
  inputStyle: {
    color: 'black',
    fontWeight: 'bold'
  }
});