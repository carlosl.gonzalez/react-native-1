import React from 'react';
import { Alert, StyleSheet, Text, View, ScrollView, Button, TouchableHighlight } from 'react-native';
import { SideMenu, List, ListItem, Header } from 'react-native-elements';
import { Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Helpers, { DPicker, Title } from '../Helpers/Helpers';
import { COLOR } from 'react-native-material-ui';

const px2dp = Helpers.px2dp;

export default class Pronostics extends React.Component {
  constructor (props) {
    super (props);
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{flex: 2, backgroundColor: COLOR.snackbarColor}}>
          <Title text="Lista de pronosticadores"/>
        </View>
        <View style={{flex:12, flexDirection: 'column', justifyContent:'center', backgroundColor: COLOR.snackbarColor}}>
          <ScrollView collapsable={true}>
            <PronosticList/>
          </ScrollView>
        </View>
        <View  style={{flex: 1, padding: 5, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', backgroundColor: 'white'}}>
          <View style={{flex: 1, padding: 5}}>
            <Button title="Boton1" color={COLOR.snackbarColor} onPress={() => console.log("Epa")}/>
          </View>
          <View style={{flex: 1, padding: 5}}>
            <Button title="Boton2" color={COLOR.snackbarColor} onPress={() => console.log("Epa")}/>
          </View>          
        </View>
      </View>
    );
  }
}

class PronosticList extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
      list: new Array(5).fill(new Pronostic({}))
    }
  }

  render () {
    let currentList = this.state.list.map( (pronostic, index) => (      
      <PronosticView key={1000 + index} pronostic={pronostic}/>      
    ));
    return (
      <View style={{flexDirection: 'column', justifyContent: 'space-between'}}>
        { currentList }           
      </View>
    );
  }
}

class PronosticView extends React.Component {
  constructor (props) {
    super(props);
  }
  showPronostics(dateStr, pronostic) {
    Alert.alert(`Pronostico: ${dateStr}`, `Mañana: ${[1,2,3]} \n Tarde:${[1,2,3]}  \n Noche:${[1,2,3,4]} `);
  }
  render() {
    let { author, lotery, pronostic, dateStr } = this.props.pronostic;
    return (
      <View style={{flex:2, backgroundColor: COLOR.grey300, flexDirection: 'row', borderWidth: 4, borderColor: COLOR.snackbarColor, justifyContent: 'space-around'}}>
        <View style={{flex:1, padding: 10, justifyContent: 'center', alignContent:'center'}}>
          <Icon name="question-circle" size={px2dp(80)}/>
          <StarRating stars={Math.floor(1000*Math.random())%6}/>
        </View>
        <View style={{flex:4, flexDirection: 'column', justifyContent: 'space-around', alignItems: 'flex-start'}}>
          <Text>
              { author }
          </Text>
          <Text>
              { lotery }
          </Text>
          <View>
            <TouchableHighlight onPress={()=>this.showPronostics(dateStr, pronostic)}>
              <Text>
                CustomButton
              </Text>
            </TouchableHighlight>
          </View>        
        </View>
      </View>
    );
  }
}
function StarRating (props) {
  let { stars } = props, aux = new Array(stars).fill( null ).map((e, index) => (
     <Icon name="star" key={index} color={COLOR.orange600}/>
  ));
  return (
    <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
      { aux }
    </View>
  ); 
}
function Pronostic ({name, lotery, result, dateStr, sort}) {
  this.author = name || 'Carlos';
  this.lotery = lotery || 'LotoActivo';
  this.pronostic = {
    results: {
      m: [],
      t: [],
      n: []
    },
    dateStr: dateStr || new Date().toLocaleDateString()
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: COLOR.grey300,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});