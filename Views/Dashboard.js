import React from 'react';
import { Alert, StyleSheet, Text, View, Dimensions } from 'react-native';
import { SideMenu, List, ListItem, Header } from 'react-native-elements'
import { COLOR } from 'react-native-material-ui';
import TabNavigator from 'react-native-tab-navigator';
import Icon from 'react-native-vector-icons/FontAwesome'
import Pronostics from './Pronostics'
import Home  from './Home'
import VIP from './VIP'
import Editor from './Editor'
import Profile from './Profile'
import Helpers from '../Helpers/Helpers';


const { px2dp } = Helpers;
export default class Dashboard extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
      selectedTab: 'pronosticos',
      isAdmin: true
    }
  }
  changeTab (selectedTab) {
    this.setState({ selectedTab });
  }
  render() {
    const { selectedTab } = this.state;
    return (
      <TabNavigator style={styles.container}>
        <TabNavigator.Item
          selected={this.state.selectedTab === 'home'}
          title="Resultados"
          selectedTitleStyle={{color: "#3496f0"}}
          renderIcon={() => <Icon name="line-chart" size={px2dp(22)} color="#666"/>}
          renderSelectedIcon={() => <Icon name="line-chart" size={px2dp(22)} color="#3496f0"/>}
          //badgeText="1"
          onPress={() => this.setState({selectedTab: 'home'})}>
          <Home/>
        </TabNavigator.Item>
        <TabNavigator.Item
          selected={this.state.selectedTab === 'pronosticos'}
          title="Pronosticos"
          selectedTitleStyle={{color: "#3496f0"}}
          renderIcon={() => <Icon name="money" size={px2dp(22)} color="#666"/>}
          renderSelectedIcon={() => <Icon name="money" size={px2dp(22)} color="#3496f0"/>}
          onPress={() => this.setState({selectedTab: 'pronosticos'})}>
          <Pronostics/>
        </TabNavigator.Item>
        <TabNavigator.Item
          selected={this.state.selectedTab === 'profile'}
          title="Mi perfil"
          selectedTitleStyle={{color: "#3496f0"}}
          renderIcon={() => <Icon name="user" size={px2dp(22)} color="#666"/>}
          renderSelectedIcon={() => <Icon name="user" size={px2dp(22)} color="#3496f0"/>}
          onPress={() => this.setState({selectedTab: 'profile'})}>
          <Profile logout={this.props.logout}/>
        </TabNavigator.Item>
        <TabNavigator.Item
          selected={this.state.selectedTab === 'vip'}
          title="VIP"
          selectedTitleStyle={{color: "#3496f0"}}
          renderIcon={() => <Icon name="star" size={px2dp(22)} color="#666"/>}
          renderSelectedIcon={() => <Icon name="star" size={px2dp(22)} color="#3496f0"/>}
          onPress={() => this.setState({selectedTab: 'vip'})}>
          <VIP/>
        </TabNavigator.Item>
        {this.state.isAdmin && <TabNavigator.Item
          selected={this.state.selectedTab === 'editor'}
          title="Editor"
          selectedTitleStyle={{color: "#3496f0"}}
          renderIcon={() => <Icon name="edit" size={px2dp(22)} color="#666"/>}
          renderSelectedIcon={() => <Icon name="edit" size={px2dp(22)} color="#3496f0"/>}
          onPress={() => this.setState({selectedTab: 'editor'})}>
          <Editor/>
        </TabNavigator.Item>}
      </TabNavigator>
    );
  }    
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }
});