import React, { Component } from 'react';
import { Alert, StyleSheet, Text, View, TextInput } from 'react-native';
import { Button } from 'react-native-elements'
//import styles from '../Css/Styles'
import Service from '../Factory/Services'
import Title from './TitleComponent'
import { SocialIcon } from 'react-native-elements'

export default class Login extends React.Component { 
  constructor (props) {
    super (props);
    this.state = {
      username: '',
      password: ''
    };
    this.service = new Service();
  };
  render () {
    return (
      <View style={{paddingTop: 50}}>
        <Title img="default"/>
        <LoginForm 
          setName={this.setName.bind(this)} 
          setPassword={this.setPassword.bind(this)} 
          verify={this.sendLogin.bind(this)}
          setView={this.props.setView} 
          onError={this.props.onError}
        />
      </View>
    );
  } 
  setName (name) {
    this.setState({username: name});
  }
  setPassword (pass) {
    this.setState({password: pass});
  }
  sendLogin () {
    let postData;
    if (!this.state.username || !this.state.password) {
      Alert.alert("Todos los campos son requeridos.");
      return;
    }
    this.props.isLoading(true);
    postData = {
      username: this.state.username,
      password: this.state.password,
    };
    this.service.api().Login(postData)
    .then((res) => {
      console.log("Esta es la respuesta", res);
      this.props.isLoading(false);
      this.props.updateUserInfo({
        user: {name: "Todo fino"},
        token: 'token',
        userIsLoggedIn: true
      });
      this.props.setView('Dashboard');
    }).catch((err) => {
      this.props.isLoading(false);
      return <Error err="Error"/>;
    });
  }  
} 
function LoginForm (props) {
  return (
    <View style={{padding: 20}}>
      <TextInput 
        style={{height: 60}}
        placeholder="Usuario"
        onChangeText={(text) => props.setName(text)}
      />   
      <TextInput 
        style={{height: 60}}
        placeholder="Contraseña"
        secureTextEntry={true}
        onChangeText={(text) => props.setPassword(text)}
      />        
      <View style={{marginTop: 25, flexDirection: 'row', justifyContent: 'center'}}>
        <Button title="Inicia sesión" onPress={() => props.verify()} 
                rounded={true} buttonStyle={{backgroundColor: 'white', elevation: 5, opacity: 0.5}}
                textStyle={{fontWeight: 'bold', fontSize: 18, color: 'black'}}/>
        <Button title="Regístrate" onPress={() => props.setView('Register')}
                rounded={true} buttonStyle={{backgroundColor: 'white', elevation: 5, opacity: 0.5}}
                textStyle={{fontWeight: 'bold', fontSize: 18, color: 'black'}}/>
      </View>
      <View style={{marginTop: 10, flex: 1}}>
        <View style={{flex:1, flexDirection: 'row', justifyContent: 'center'}}>
          <SocialIcon type='twitter'/>
          <SocialIcon type='facebook'/>
          <SocialIcon type='instagram'/>
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  buttonContainer: {
    margin: 20
  },
  alternativeLayoutButtonContainer: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});