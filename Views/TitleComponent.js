import React, { Component } from 'react';
import { Alert, StyleSheet, Text, View, TextInput, Button } from 'react-native';
import { COLOR } from 'react-native-material-ui';
export default class Title extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
      titleFrame: '',
      title: 'Aplicacion de prueba',
      slice: 1
    }
  }
  render () {
    return (
      <View style={styles.titleContainer}>
        <View style={styles.title}>
            <Text style={styles.appTitle}>{this.state.title}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleContainer: {
    height:200, 
    backgroundColor:COLOR.grey400,
  },
  title: {
    flex: 1, 
    flexDirection: 'column', 
    justifyContent: 'space-around'
  },
  appTitle: {
    color: 'white',
    textAlign: 'center',
    fontSize: 40,
    fontWeight: 'bold',
    textShadowColor: 'green',
    textShadowRadius: 3
  }
});