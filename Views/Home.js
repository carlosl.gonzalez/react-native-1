import React from 'react';
import { Animated, Alert, StyleSheet, View, Dimensions, Modal, TouchableHighlight, Image, PanResponder, StatusBar } from 'react-native'
import { SideMenu, List, ListItem, Header, Text, Badge, Button, ButtonGroup } from 'react-native-elements';
import TabNavigator from 'react-native-tab-navigator';
import Service from '../Factory/Services';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Icon as Icon2 } from 'react-native-elements';
import { Dialog, DialogDefaultActions, COLOR, ThemeProvider } from 'react-native-material-ui';
import SWAL from './CustomAlert';
import Helpers, { Title, DPicker } from '../Helpers/Helpers';
import AnimalIcons from '../Helpers/AnimalIcons';
import DatePicker from 'react-native-datepicker';

const { px2dp } = Helpers;

class Home extends React.Component {
  constructor (props) {
    super (props);
    this.service = new Service();
    this.state = {
      error: '',
      yesterday: {},
      today: {},
      isLoading: false,
      selectedIndex: 0
    }
    this.updateIndex = this.updateIndex.bind(this)
  }
  updateIndex (selectedIndex) {
    this.setState({selectedIndex})
  }
  loading(isLoading) {
    this.setState({ isLoading });
  }
  fetchResults() {
    this.loading(true);
    this.service.api().GetResults()
    .then((res) => {
      console.log("Respuesta de GetResults", res);
      this.loading(false);
      let { yesterday, today } = res;
      this.setState({ yesterday, today });
    }).catch((err) => {
      this.loading = false;
      this.setState({
        error: 'No se pudieron obtener los resultados.'
      });
    });
  }
  componentDidMount() {
    this.fetchResults();
  }
  
  render() {
    const { today, selectedIndex, yesterday } = this.state;
    let m,n,t;
    const buttons = ['Hoy', 'Ayer', 'Otros'];
    
    if (this.state.error) {
      return (
        <View style={styles.errorContainer}>
          <StatusBar hidden />
          <Error err={this.state.error} clearError={() => this.setState({error: ''})}/>
        </View>
      );
    } else {
      var showDateFilter = false;
      if (selectedIndex == 2) {
        showDateFilter = true;
        return ( 
          <View style={styles.container} >
            <StatusBar hidden />
            <ButtonGroup 
              onPress={this.updateIndex}
              selectedIndex={selectedIndex}
              buttons={buttons}
              containerStyle={{height: 40, borderColor: COLOR.snackbarColor}} 
              buttonStyle={{backgroundColor: COLOR.snackbarColor}}
              textStyle={{color: 'white', fontWeight: 'bold', fontSize: 20}}
            />
            <FindResult fetchResults={this.fetchResults.bind(this)}/>            
          </View>
        );
      }
      const currentList = selectedIndex%2 == 0 ? today : yesterday; 
      if (!currentList) {
        return (
          <View style={styles.container}>
            <StatusBar hidden />
            <ButtonGroup 
              onPress={this.updateIndex}
              selectedIndex={selectedIndex}
              buttons={buttons}
              containerStyle={{height: 40, borderColor: COLOR.snackbarColor}} 
              buttonStyle={{backgroundColor: COLOR.snackbarColor}}
              textStyle={{color: 'white', fontWeight: 'bold', fontSize: 20}}
            />
            <View style={{flex:1, justifyContent: 'center', backgroundColor: COLOR.grey300}}>
              <TouchableHighlight onPress={()=> this.fetchResults()}>
                <Title text="No hay resultados para la fecha" color={ COLOR.blue400 }/>
              </TouchableHighlight>
            </View>
          </View>
        );
      }
      if (currentList.list) {
        ({m,n,t} = currentList.list)
      }
      return (currentList && currentList.list && (m || t || n)) ? (
        <View style={styles.container} >
          <StatusBar hidden />
          <ButtonGroup 
            onPress={this.updateIndex}
            selectedIndex={selectedIndex}
            buttons={buttons}
            containerStyle={{height: 40, borderColor: COLOR.snackbarColor}} 
            buttonStyle={{backgroundColor: COLOR.snackbarColor}}
            textStyle={{color: 'white', fontWeight: 'bold', fontSize: 20}}
          />
          <DayResult list={currentList.list} date={currentList.date} fetchResults={this.fetchResults.bind(this)}/>          
        </View>
      ) : (
        <View style={styles.container} >
          <StatusBar hidden />
          <ButtonGroup 
            onPress={this.updateIndex}
            selectedIndex={selectedIndex}
            buttons={buttons}
            containerStyle={{height: 40, borderColor: COLOR.snackbarColor}} 
            buttonStyle={{backgroundColor: COLOR.snackbarColor}}
            textStyle={{color: 'white', fontWeight: 'bold', fontSize: 20}}
          />
          <View style={{backgroundColor: 'red'}}>
            <Text> No hay resultados </Text>
          </View>
        </View>
      );
    }
  }
} 

class FindResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: null,
      list: {m: [], t:[], n:[]}
    }
  }
  validDate (date) {
    this.setState({ date });
    let service = new Service();
    service.api().GetResultsByDate({date: Helpers.formatDate(date)})
    .then(({ list }) => {
      console.log("Respuesta de GetResultsByDate", list)
      this.setState({ list });
    }).catch((err) => {
      Alert.alert("Error en la llamada");
    });
  }
  render() {
    return (
      <View style={styles.resultsContainer}>
        <View style={{flex: 1, backgroundColor: COLOR.grey300, alignItems:'center', justifyContent:'center'}}>
          <DPicker 
            minDate={new Date(new Date().getTime() - 1*30*24*3600000)} 
            onChange={this.validDate.bind(this)}
          />
        </View>
        <View style={{flex: 9, backgroundColor: COLOR.grey300, padding: 10, borderColor: 'black'}}>
          {!!this.state.date && <DayResult list={this.state.list} date={this.state.date} fetchResults={() => this.props.fetchResults()}/>}  
          {!this.state.date &&  <Warning/>}
        </View>
      </View>
    );
  }
}

function Warning (props) {
  return (
    <View style={{flex:1, flexDirection: 'column', justifyContent: 'center'}}>
      <View style={{backgroundColor: COLOR.snackbarColor, borderRadius: 20, padding: 10}}>
        <Text style={{textAlign: 'center', fontSize: 25, fontWeight: 'bold', color: 'white'}}>
          Consulta por día
        </Text>
      </View>
    </View>
  );
}

class DayResult extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      modalVisible: false,
      modalMessage: ''
    };
  }
  toggleModal (message) {
    this.setState({ 
      modalVisible: !!message,
      modalMessage: message 
    });
  }
  closeModal () {
    this.setState({modalMessage: ''});
  }
  render () {
    let {m, n, t} = this.props.list,
        labels = [
          "Mañana",
          "Tarde",
          "Noche"
        ],
        hours = [
          "9:00 am, 10:00 am, 11:00 am".split(", "),
          "12:00 m, 1:00 pm, 3:00 pm".split(", "),
          "4:00 pm, 5:00 pm, 6:00 pm, 7:00 pm".split(", ")
        ],
        allResults = [m, t, n].map((elem, index) => {
          return (
            <View style={{flex: 2, padding: 20}} key={index}>
              <ListResults list={ elem } hours={ hours[index] } onTouch={this.toggleModal.bind(this)}/>
            </View>
          );
        });
    return (
      <View style={styles.resultsContainer}>
        {
          !!this.state.modalMessage && 
          <Stats2 animal={this.state.modalMessage} closeModal={this.closeModal.bind(this)}/>
        }
        {
          !this.state.modalMessage && 
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>  
            <Text style={{fontWeight: 'bold', textAlign: 'center', fontSize: 18, marginTop: 5}}>
              {this.props.date}
            </Text> 
            {
              this.props.fetchResults && 
              <Icon2 
                name="refresh" type="font-awesome" 
                containerStyle={{paddingLeft: 10}} 
                onPress={()=>this.props.fetchResults()}
              />
            }            
          </View>
        }
        {
          !this.state.modalMessage && allResults 
        }
      </View>
    );
  }
}

class Stats extends React.Component {
  constructor (props) {
    super (props);
  }
  updateState (obj) {
    this.setState(obj)
  }
  render () {
    return (
      <View>
        <View>
          <StatusBar hidden/>
        </View>
        <View> 
          <Modal 
            animationType="fade" transparent = {true}
            onRequestClose = {() => { console.log("Modal has been closed.") } }
          >      
            <View style={{backgroundColor: COLOR.grey700, flex:1, flexDirection: 'column'}}>  
              <View style={{flex: 1, alignItems: 'center'}}>
                <View style={{}}>
                  <AnimalIcons 
                    animal={this.props.animal} 
                    iconStyle={{borderRadius: 10, width: px2dp(150), height: px2dp(150)}}
                    onTouch={() => {}}
                  />
                </View>
              </View>
              <View style={{flex:5}}>
                <Information animal={this.props.animal}/>
              </View>
              <View style={{flex:1, justifyContent:'center' }}>
                <TouchableHighlight>
                  <Button rounded title="Aceptar" onPress={() => this.props.closeModal()}></Button>
                </TouchableHighlight>
              </View>
            </View>
          </Modal>
        </View>
      </View>
    )
  }
}

class Stats2 extends React.Component {
  constructor (props) {
    super (props);
  }
  updateState (obj) {
    this.setState(obj)
  }
  render () {
    return (
      <View>
      <View>
        <StatusBar hidden/>
      </View>
      <View style={{flex:1, flexDirection: 'column'}}> 
      <FadeInView style={{width: Helpers.wDim().w,  height: Helpers.wDim().h-100, backgroundColor: 'powderblue'}}>      
        <View style={{backgroundColor: COLOR.grey700, flex:1, flexDirection: 'column'}}>  
          <View style={{flex: 1, alignItems: 'center'}}>
            <View style={{}}>
              <AnimalIcons 
                animal={this.props.animal} 
                iconStyle={{borderRadius: 10, width: px2dp(150), height: px2dp(150)}}
                onTouch={() => {}}
              />
            </View>
          </View>
          <View style={{flex:5}}>
            <Information animal={this.props.animal}/>
          </View>
          <View style={{flex:1, justifyContent:'center' }}>
            <TouchableHighlight>
              <Button rounded title="Aceptar" onPress={() => this.props.closeModal()}></Button>
            </TouchableHighlight>
          </View>
        </View>
      </FadeInView>
      </View>
      </View>
    )
  }
}

class Information extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
      message: '',
      byDayOfWeek: {},
      byPosition: {},
      lastMonth: {},
      lastWeek: {}
    }
  }
  componentDidMount () {
    let service = new Service();
    service.api().GetStats({animal: this.props.animal})
    .then((res) => {
      let { byDayOfWeek, byPosition, lastMonth, lastWeek } = res;
      this.setState({ byDayOfWeek, byPosition, lastMonth, lastWeek });
    }).catch((err) => {
      console.log("Error en la respuesta", err);
    });
  }
  render () {
    return (
      <View style={{flex: 1, flexDirection: 'column', justifyContent: 'space-around'}}>
        <Text> EN CONSTRUCCION </Text>
      </View>
    )
  }
}

function ListResults (props) {
  let { list, hours } = props,
      mapping  = list.map(function(elem, index){
        return <Result key={index} info={elem} hour={hours[index]} onTouch={props.onTouch}/>
      });
  
  return (
    <View style={styles.sortContainer}>
      { mapping }
    </View>
  );
}

class Result extends React.Component {
  constructor (props) {
    super (props);
    this.showMessage = false;
    this.state = {
      showMessage: false
    }
  }  
  render () {    
    let { info, hour, onTouch } = this.props;
    return (
      <View style={{flex:1, flexDirection: 'column', justifyContent: 'center'}}>
        <View style={{alignItems: 'center'}}>
        <AnimalIcons           
          animal={info}  
          onTouch={onTouch}          
        />
        <Text style={{textAlign: 'center'}}>{ hour }</Text>
        </View>
      </View>
    );
  }
}

class FadeInView extends React.Component {
  
  state = {
    fadeAnim: new Animated.Value(0),  // Initial value for opacity: 0
  }
  componentDidMount() {
    Animated.timing(                  // Animate over time
      this.state.fadeAnim,            // The animated value to drive
      {
        toValue: 1,                   // Animate to opacity: 1 (opaque)
        duration:500,              // Make it take a while
      }
    ).start();                        // Starts the animation
  }

  render() {
    let { fadeAnim } = this.state;
    return (
      <Animated.View                 // Special animatable View
        style={{
          ...this.props.style,
          opacity: fadeAnim,         // Bind opacity to animated value
        }}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}

const uiTheme = {
  palette: {
    primaryColor: COLOR.blue500,
  },
  dialog: {
    container: {
      height: px2dp(200),
      backgroundColor: COLOR.grey300,
      justifyContent: 'center',
      flexDirection: 'column',
    }
  }
};
function Error (props) {
  if (!props.err)
    return <Text></Text>;
  return (
    <ThemeProvider uiTheme={uiTheme}>
    <View >
      <Dialog>
        <Dialog.Title><Text>Error en la respuesta</Text></Dialog.Title>
        <Dialog.Content>
          <Text>
            {props.err}
          </Text>
        </Dialog.Content>
        <Dialog.Actions>
          <DialogDefaultActions
             actions={['Aceptar']}
             onActionPress={(a) => props.clearError({error: ''})}
          />
        </Dialog.Actions>
      </Dialog>
    </View>
    </ThemeProvider>
  );
}

function Message (props) {
  if (!props.msg)
    return <Text></Text>;
  return (
    <ThemeProvider uiTheme={uiTheme}>
    <View >
      <Dialog>
        <Dialog.Title><Text>Dataso</Text></Dialog.Title>
        <Dialog.Content>
          <Text>
            {props.err}
          </Text>
        </Dialog.Content>
        <Dialog.Actions>
          <DialogDefaultActions
             actions={['Aceptar']}
             onActionPress={() => props.clearMessage()}
          />
        </Dialog.Actions>
      </Dialog>
    </View>
    </ThemeProvider>
  );
}

const styles = StyleSheet.create({
  errorContainer: {
    flex: 1,
    backgroundColor: COLOR.grey300,
    paddingTop: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1,
    backgroundColor: COLOR.snackbarColor,
    paddingTop: 5,      
  },
  resultsContainer: {
    flex: 1,
    backgroundColor: COLOR.grey300,
    flexDirection: 'column'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  modalInner: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: COLOR.grey600,
      padding: 100
  },
  text: {
      color: COLOR.white,
      marginTop: 10
  },
  modalText: {
      color: COLOR.white,
      marginTop: 10,
      fontSize: 30
  },
  sortContainer: {
    flexDirection: 'row', 
    justifyContent: 'space-around', 
    backgroundColor: "#CCFFE5", 
    padding: 10, 
    borderRadius: 10
  }
});


export { Home as default, DayResult };