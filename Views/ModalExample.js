import React from 'react';
import { Alert, StyleSheet, Text, View, Modal, Button } from 'react-native';


export default class Dashboard extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
      modalVisible: false
    }
  }
  openModal() {
    this.setState({modalVisible:true});
  }
  closeModal() {
    this.setState({modalVisible:false});
  }
  render() {
    return (
      <View style={styles.dashboard}>
        <Modal
          visible={this.state.modalVisible}
          animationType='fade'
          onRequestClose={() => this.closeModal()}
        >
          <View style={styles.modalContainer}>
            <View style={styles.innerContainer}>
              <Text>This is content inside of modal component</Text>
              <Button
                onPress={() => this.closeModal()}
                title="Close modal"
              >
              </Button>
            </View>
          </View>
        </Modal>
        <Button
            onPress={() => this.openModal()}
            title="Open modal"
        />
      </View>
    );
  }    
}

const styles = {
  dashboard: {
    flex: 1,
    paddingTop: 30,
    justifyContent: 'center',
    
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'grey',
    
  },
  innerContainer: {
    alignItems: 'center',
  }
}