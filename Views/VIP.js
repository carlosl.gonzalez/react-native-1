import React from 'react';
import { Alert, StyleSheet, Text, View } from 'react-native';
import { SideMenu, List, ListItem, Header } from 'react-native-elements'
import TabNavigator from 'react-native-tab-navigator';
import {Dimensions} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class VIP extends React.Component {
  constructor (props) {
    super (props);
  }
  render() {
    return (
    <View style={styles.container}>
      <Text style={styles.welcome}>
        Zona VIP
      </Text>
    </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});