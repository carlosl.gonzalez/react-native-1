import React from 'react';
import { Text, View, Button, TextInput, ScrollView, Alert } from 'react-native';
import { COLOR } from 'react-native-material-ui';
import AnimalIcons from '../Helpers/AnimalIcons';
import Helpers, { DPicker } from '../Helpers/Helpers';
import DatePicker from 'react-native-datepicker';
import Service from '../Factory/Services';

const { px2dp } = Helpers;

export default class Editor extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      results : {
        "9:00 am": null,
        "10:00 am": null,
        "11:00 am": null,
        "12:00 m": null,
        "1:00 pm": null,
        "3:00 pm": null,
        "4:00 pm": null,
        "5:00 pm": null,
        "6:00 pm": null,
        "7:00 pm": null,
      },
      selectedKey: null,
      selectedValue: null,
      date: null,
      dateStr: new Date().toLocaleDateString()
    }
  }
  updateKey (selectedKey) {
    this.setState({ selectedKey });
  }
  fetchResults (date) {
    let dateStr = Helpers.formatDate(date);
    let service = new Service();
    this.setState({ dateStr, date });
    service.api().GetResultsByDate({date: dateStr, form: 'plain'})
    .then(({ results }) => {
      this.setState({ date, results })
    }).catch((err) => {
      console.log("Error en la llamda", err);
      Alert.alert("Error en la llamada");
    });
  }

  updateResults () {
    console.log("Enviando resultados al servidor");
    let service = new Service();
    let { selectedKey, selectedValue, ...postData } = this.state;
    service.api().UpdateResults(postData)
    .then((res) => {
      console.log("Respuesta del servicio", res);
      Alert.alert("Actualización exitosa");
    })
    .catch((err) => {
      console.log("Error en la llamada", err);
      Alert.alert("Error en la llamada");
    });
  }
    
  render () {
    let size = 70;
    let textoLoco = Helpers.getAnimals().split("|").map((e,i) => (
      <View key={i} style={{flexDirection: 'row', justifyContent: 'space-around', paddingTop: 5}}>
        {
          e.split(",").map((elem, index) => (
            <View key={index + 100}>
              <AnimalIcons 
                animal={elem} 
                iconStyle={{width: px2dp(size), height: px2dp(size)}}
                onTouch={() => {
                  console.log("seleccionaste " + elem + " para las " + this.state.selectedKey)
                  let { results } = this.state;
                  results[this.state.selectedKey] = elem;
                  this.setState({ selectedValue: elem, results });                  
                  console.log(this.state)
                }}
              />
            </View>
          ))
        }        
      </View>
    ));
    return (
      <View style={{flex: 1, backgroundColor: COLOR.grey300, justifyContent: 'space-around'}}>
        <View style={{flex: 3, flexDirection: 'row', backgroundColor: COLOR.grey300, padding: 10, justifyContent: 'space-around', alignItems: 'center'}}>   
          <DPicker 
            minDate={new Date(new Date().getTime() - 1*30*24*3600000)}
            onChange={this.fetchResults.bind(this)}
          />
        </View>
        <View style={{flex: 10, backgroundColor: COLOR.grey300}}>
          <ScrollView style={{padding: 10}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <InputGrid rows={3} cols={3} onPress={this.updateKey.bind(this)} results={this.state.results}/>
            </View>
          </ScrollView>
        </View>
        <View style={{borderColor:COLOR.snackbarColor, borderWidth: 2,}}/>
        <View style={{flex: 20, backgroundColor: 'white'}}>
          <ScrollView style={{padding: 10}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              {textoLoco}
            </View>
          </ScrollView>
        </View>
        <View style={{flex: 2, backgroundColor: COLOR.grey300, justifyContent: 'center', padding: 20}}>
          <Button title="Guardar" color={COLOR.snackbarColor} onPress={() => this.updateResults()}/>
        </View>
      </View>      
    );
  }
}

class InputGrid extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      selectedKey: null      
    }
  }

  render () {
    let { rows, cols } = this.props, counter = 0;
    let matrixRows = new Array(rows).fill(null),
        hours = [].concat.apply([], [
          "9:00 am, 10:00 am, 11:00 am".split(", "),
          "12:00 m, 1:00 pm, 3:00 pm".split(", "),
          "4:00 pm, 5:00 pm, 6:00 pm, 7:00 pm".split(", ")
        ]).reverse();
    let _newRows = matrixRows.map((elem, index) => {
      let columns = new Array(cols).fill(null);
      let _newColumns = columns.map((el, idx) => {
        counter++;
        let aux = hours[hours.length - counter];
        return (
          <View style={{padding: 10, width: 100}}  key={(index + 1) * (idx + 1)}>
            <Button 
              color={aux==this.state.selectedKey ? 'blue' : COLOR.snackbarColor} 
              title={hours[hours.length - counter]} 
              onPress={() => { 
                this.props.onPress(aux);
                this.setState({
                  selectedKey: aux
                });
              }}
            />
            <Text style={{textAlign: 'center'}}>{this.props.results[aux]}</Text>
          </View>
        );
      });
      return (
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}} key={20-index}>
          { _newColumns }
        </View>
      );
    });
    return (
      <View style={{flexDirection: 'column', justifyContent: 'space-around'}}>
        { _newRows }
        <View style={{ padding: 10, width: 100 }}>
          <Button 
            color={"7:00 pm"==this.state.selectedKey ? 'blue' : COLOR.snackbarColor} 
            title="7:00 pm" 
            onPress={() => { 
              this.props.onPress("7:00 pm");
              this.setState({
                selectedKey: "7:00 pm"
              });
            }}
          />
          <Text style={{textAlign: 'center'}}>{this.props.results["7:00 pm"]}</Text>
        </View>
      </View>
    );
  }
}

//db.plantillas.updateOne({_id:  ObjectId("5a833fec20f95508ab031355")},{$set:{ Family: "01:5a833cae20f95508ab03128d:RCTST0000006197", "MerchantInfo.MID": "RCTST0000006197"}})