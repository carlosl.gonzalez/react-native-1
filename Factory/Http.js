export default function Http (opt) {
    return {
        get: _get,
        post: _post,
        put: _put
    }
}
function _get (url) {
    if (!url) {
        return Promise.reject("Must provide url");
    }
    return fetch(url, {                        
        method: 'GET'
    }).then(complete).catch(failed);
}
function _post(url, postData) {
    if (!url) {
        return Promise.reject("Must provide url");
    }
    return fetch(url, {                        
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(postData || {})
    }).then(complete).catch(failed);
}
function _put(url, postData) {
    if (!url) {
        return Promise.reject("Must provide url");
    }
    return fetch(url, {                        
        method: 'PUT',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(postData || {})
    }).then(complete);
}
function complete (res) {
    return JSON.parse(res._bodyText).message;
}
function failed (err) {
    console.log("Hubo un error", err);
    throw new Error(err);
}
//0052300177850