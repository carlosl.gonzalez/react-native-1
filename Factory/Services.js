import Http from './Http'

export default class Service {
  constructor (endPoint) {
    this.endPoint = 'http://192.168.43.98:3003/api';
    //this.endPoint = 'http://192.168.0.121:3003/api';
  }
  api () {
    let url = this.endPoint;
    return {
      Login: (postData) => {
        return Http().post(url + '/login', postData);
      },
      Register: (postData) => {
        return Http().post(url + '/Register', postData);
      },
      GetResults: (opts) => {
        let dates = [ new Date(), new Date(Date.now() - 24 * 3600000) ].map(toLocale)
        return Http().get(url + '/GetResults?today=' + dates[0] + '&yesterday=' + dates[1]);
      },
      GetResultsByDate: (opts) => {
        let route = url + '/GetResultsByDate?date=' + opts.date;
        if (opts.form) {
          route += ('&form=' + opts.form);
        }
        return Http().get(route);
      },
      GetStats: (opts) => {
        return Http().get(url + '/GetStats?animal=' + opts.animal);
      },
      UpdateResults: (postData) => Http().post(url + '/UpdateResults', postData)
    };
  }  
}
function toLocale (date) {
  return date.toLocaleDateString();
}