import React from 'react';
import { Dimensions, Text, View } from 'react-native';
import { COLOR } from 'react-native-material-ui';
import DatePicker from 'react-native-datepicker';

const Helpers = {
  px2dp: _px2dp,
  wDim: _wDim,
  getAnimals: _getAnimals,
  formatDate: _formatDate,
}

function _formatDate (date) {
  let [ year, month, day ] = date.split("-");
  return [month,day,year % 100].join("/");
}
function _getAnimals () {
  return "aguila,alacran,ardilla,ballena|burro,caballo,caiman,camello|carnero,chivo,cienpies,cochino|culebra,delfin,elefante,gallina|gallo,gato,iguana,jirafa|lapa,leon,mono,oso|paloma,pavo,perico,perro|pescado,rana,raton,tigre|toro,vaca,venado,zamuro|zebra,zorro";
}

function _wDim () {
  return {
    w: Dimensions.get("window").width,
    h: Dimensions.get("window").height
  }
}

function _px2dp (px) {
  const deviceW = Dimensions.get('window').width;
  const basePx = 375;
  return px *  deviceW / basePx;
}

function Title (props) {
  let { color = COLOR.snackbarColor, text, textColor='white' } = props;
  return (
    <View style={{padding: 10}}>
      <Text style={{fontSize: 22, textAlign: 'center', fontWeight: 'bold', backgroundColor: color, padding: 15, borderRadius: 30, color: textColor}}> 
        { text } 
      </Text>
    </View>
  );
}


class DPicker extends React.Component {
  constructor (props) {
    super (props);
    this.state = {
      date: null
    }
  }

  onDateChange (date) {
    this.setState({ date });
    this.props.onChange(date);
  }
  render () {
    let { borderColor, fontColor, color } = this.props;
    return (
      <DatePicker
        style={{width: 300, backgroundColor: color || COLOR.grey300}}
        date={this.state.date}
        mode="date"
        placeholder="Seleccione una fecha"
        format="YYYY-MM-DD"
        confirmBtnText="Aceptar"
        cancelBtnText="Cancelar"
        maxDate={new Date()}
        minDate={this.props.minDate || new Date()} //new Date(new Date().getTime() - 1*30*24*3600000)
        customStyles={{
          dateInput: {
            borderColor: borderColor || COLOR.snackbarColor, 
            borderRadius: 20,               
          },
          dateText: {
            fontSize: 18,
            fontWeight: 'bold',
            color: fontColor || COLOR.snackbarColor
          },
          placeholderText: {
            fontSize: 18,
            fontWeight: 'bold',
            color: fontColor || COLOR.snackbarColor
          }
        }}
        onDateChange={(date) => this.onDateChange(date)}
      />
    );
  }
}

export default Helpers;
export { DPicker, Title }