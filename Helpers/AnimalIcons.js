import React from 'react';
import { Image, TouchableHighlight } from 'react-native';
import { COLOR } from 'react-native-material-ui';
import Icon from 'react-native-vector-icons/FontAwesome';
import Helpers from './Helpers'
const { px2dp } = Helpers;
const animals = {
  aguila  : require('../Assets/Iconos/aguila.jpg'),
  alacran : require('../Assets/Iconos/alacran.jpg'),
  ardilla : require('../Assets/Iconos/ardilla.jpg'),
  ballena : require('../Assets/Iconos/ballena.jpg'),
  burro   : require('../Assets/Iconos/burro.jpg'),
  caballo : require('../Assets/Iconos/caballo.jpg'),
  caiman  : require('../Assets/Iconos/caiman.jpg'),
  camello : require('../Assets/Iconos/camello.png'),
  carnero : require('../Assets/Iconos/carnero.jpg'),
  chivo   : require('../Assets/Iconos/chivo.jpg'),
  cienpies: require('../Assets/Iconos/cienpies.jpg'),
  cochino : require('../Assets/Iconos/cochino.jpg'),
  culebra : require('../Assets/Iconos/culebra.jpg'),
  delfin  : require('../Assets/Iconos/delfin.jpg'),
  elefante: require('../Assets/Iconos/elefante.jpg'),
  gallina : require('../Assets/Iconos/gallina.jpg'),
  gallo   : require('../Assets/Iconos/gallo.jpg'),
  gato    : require('../Assets/Iconos/gato.jpg'),
  iguana  : require('../Assets/Iconos/iguana.jpg'),
  jirafa  : require('../Assets/Iconos/jirafa.jpg'),
  lapa    : require('../Assets/Iconos/lapa.jpg'),
  leon    : require('../Assets/Iconos/leon.jpg'),
  mono    : require('../Assets/Iconos/mono.jpg'),
  oso     : require('../Assets/Iconos/oso.jpg'),
  paloma  : require('../Assets/Iconos/paloma.jpg'),
  pavo    : require('../Assets/Iconos/pavo.jpg'),
  perico  : require('../Assets/Iconos/perico.jpg'),
  perro   : require('../Assets/Iconos/perro.jpg'),
  pescado : require('../Assets/Iconos/pescado.jpg'),
  rana    : require('../Assets/Iconos/rana.jpg'),
  raton   : require('../Assets/Iconos/raton.jpg'),
  tigre   : require('../Assets/Iconos/tigre.jpg'),
  toro    : require('../Assets/Iconos/toro.jpg'),
  vaca    : require('../Assets/Iconos/vaca.jpg'),
  venado  : require('../Assets/Iconos/venado.jpg'),
  zamuro  : require('../Assets/Iconos/zamuro.jpg'),
  zebra   : require('../Assets/Iconos/zebra.jpg'),
  zorro   : require('../Assets/Iconos/zorro.jpg')
}
export default class AnimalIcons extends React.Component {
  constructor (props) {
    super (props);
  }

  render () {
    let { animal, iconStyle } = this.props,
        img = animals[animal];
    if (!iconStyle) {
      var width=px2dp(80), 
          height=px2dp(80),
          others={};
    } else {
      var { width=px2dp(80), height=px2dp(80), ...others } = iconStyle;
    }
    return img ? (
      <TouchableHighlight onPress={() => this.props.onTouch(animal)}>
        <Image  
          source={img} 
          style={{
            alignSelf: 'stretch',
            resizeMode: 'cover',
            width: width,
            height: height,
            borderRadius: width / 2,
            ...others
          }}
        />        
      </TouchableHighlight>
    ) : (
      <Icon name="question-circle"  size={px2dp(80)} color="#3496f0"/>
    );
  }
}